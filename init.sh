#!/bin/bash

cp -R /vagrant/buildpack/* /home/vagrant
# Using a symbolic link results in an error, so simply copy instead.
#if [[ ! -L "/home/vagrant/project" ]]; then
#  ln -s /vagrant/src /home/vagrant/project
#fi
mkdir project
cp -R /vagrant/src/* project
sudo chown -R vagrant:vagrant /home/vagrant
cp /vagrant/build.sh /home/vagrant/
sudo chmod u+x /home/vagrant/build.sh
cp /vagrant/run.sh /home/vagrant/
sudo chmod u+x /home/vagrant/run.sh
cp /vagrant/docker-compose.yml /home/vagrant/
