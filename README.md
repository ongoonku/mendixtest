Simple Mendix application to test Mendix in a Docker container.

Features:

- logging to application insights
- Mendix metrics to application insights

To run:

1. vagrant up
2. vagrant ssh
3. ./init.sh
4. ./build.sh
5. ./run.sh
6. Open a browser and point it to http://192.168.33.10:8080.
